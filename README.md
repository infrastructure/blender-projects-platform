# Blender Projects Platform

This repo is used to track issues related to development infrastructure. This includes:
* [Gitea](https://projects.blender.org)
* [Buildbot](https://builder.blender.org)
* [Wiki](https://wiki.blender.org)
* [Devtalk](https://devtalk.blender.org)
* [Subversion](https://svn.blender.org)

Read the announcement on [code.blender.org](https://code.blender.org/2023/02/new-blender-development-infrastructure/).
